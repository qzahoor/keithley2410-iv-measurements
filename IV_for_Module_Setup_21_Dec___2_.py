# -*- coding: utf-8 -*-
"""
Created on Mon Dec  29 :2022

author: Quratulain Zahoor 
Institute : National Center for Physics 
qurat.ul.ain.zahoor@cern.ch
"""

###Module Iv using keithley2410 

SaveFiles = True   # Save the plot & data?  Only display if False.
SavePath = 'F:\Quratulain\I_VMEAS\Measurement file\Sensor IV\IV for Module setup(21 Dec)'
DevName = 'I-V Curve 01' # will be inserted into filename of saved plot
Keithley_GPIB_Addr = 24


CurrentCompliance = 10.0e-6    # compliance (max) current
start = -0.0     # starting value of Voltage sweep
stop = -300.0      # ending value 
numpoints = 100  # number of points in sweep

#Important Python code to execute 
import os.path
import visa          # PyVISA module, for GPIB comms
import pyvisa as visa
import numpy as N    # enable NumPy numerical analysis
import time          # to allow pause between measurements
import os            # Filesystem manipulation - mkdir, paths etc.
import pyvisa
import numpy as np

import matplotlib.pyplot as plt # for python-style plottting, like 'ax1.plot(x,y)'

SavePath = 'F:\Quratulain\I_V MEAS\Measurement file\Sensor IV'
#Open Visa connections to instruments
#keithley = visa.GpibInstrument(24)     # GPIB addr 22

rm = visa.ResourceManager()
print(rm.list_resources())
keithley = rm.open_resource('GPIB0::24::INSTR')
print(keithley.query(":*IDN?"))



# Setup electrodes as voltage source
keithley.write("*RST")
#print("reset the instrument")
time.sleep(0.5)    # add second between
keithley.write(":SOUR:FUNC:MODE VOLT")
keithley.write(":SENS:CURR:PROT:LEV " + str(CurrentCompliance))
keithley.write(":SENS:CURR:RANGE:AUTO 1")   # set current reading range to auto (boolean)
keithley.write(":OUTP ON")                    # Output on    


# Loop to sweep voltage to ramp up the voltage 

# Setup electrodes as voltage source
keithley.write("*RST")
#print("reset the instrument")
time.sleep(0.5)    # add second between
keithley.write(":SOUR:FUNC:MODE VOLT")
keithley.write(":SENS:CURR:PROT:LEV " + str(CurrentCompliance))
keithley.write(":SENS:CURR:RANGE:AUTO 1")   # set current reading range to auto (boolean)
keithley.write(":OUTP ON")                    # Output on    
##Rampup##

Voltage=[]
Current = []
for V in N.linspace(start, stop, num=numpoints, endpoint=True):
    #Voltage.append(V)
    print("Voltage set to: " + str(V) + " V" )
    keithley.write(":SOUR:VOLT " + str(V))
    time.sleep(1)    # add second between
    data = keithley.query(":READ?")   #returns string with many values (V, I, ...)
    answer = data.split(',')    # remove delimiters, return values into list elements
    I = eval( answer.pop(1) ) * 1e9     # convert to number
    Current.append( I )
    
    vread = eval( answer.pop(0) )
    Voltage.append(vread)
    #Current.append(  I  )          # read the current
    
    print("--> Current = " + str(Current[-1]) + ' nA') 
    if V<= -200: ## we can add the current value if I == 100e-9A(its optional)
        break
    #print("out of loop")
    
#keithley.write(":OUTP OFF")     # turn off
## Code For Showing ramp up##
    print("out of loop")


#print("now start ramp down")
## Ramop Down###

startD = V     # starting value of Voltage sweep
stopD = -0.0      # ending value 


Voltage=[]
Current = []
for V in N.linspace(startD, stopD, num=numpoints, endpoint=True):
    #Voltage.append(V)
    print("Voltage set to: " + str(V) + " V" )
    keithley.write(":SOUR:VOLT " + str(V))
    time.sleep(1)    # add second between
    data = keithley.query(":READ?")   #returns string with many values (V, I, ...)
    answer = data.split(',')    # remove delimiters, return values into list elements
    I = eval( answer.pop(1) ) * 1e9     # convert to number
    Current.append( I )
    
    vread = eval( answer.pop(0) )
    Voltage.append(vread)
    #Current.append(  I  )          # read the current
    
    print("--> Current = " + str(Current[-1]) + ' nA') 
    
keithley.write(":OUTP OFF")     # turn off






    
    




###### Plot #####
    
fig1, ax1 = plt.subplots(nrows=1, ncols=1)         # new figure & axis

line1 = ax1.plot(Voltage, Current, 'g+-')

ax1.set_xlabel('Voltage (V)')
ax1.set_ylabel('Current (nA)')

ax1.set_title('Module IV Measurement using keithley - ' + DevName)


fig1.show()  # draw & show the plot - unfortunately it often opens underneath other windows
fig1.canvas.window().raise_()

N.savetxt('IVdata1.csv', [p for p in zip(Voltage, Current)], delimiter=',', fmt='%s')



if SaveFiles:
    # create subfolder if needed:
    if not os.path.isdir(DevName): os.mkdir(DevName)
    curtime = time.strftime('%Y-%M-%d_%H%M.%S')
    SavePath = os.path.join(DevName, 'I-V Curve - ' + DevName + ' - [' + curtime +']' )
    fig1.savefig(  SavePath + '.png'  )
    N.savetxt('data2.csv', [p for p in zip(Voltage, Current)], delimiter=',', fmt='%s')


   
end if(SaveFiles)

